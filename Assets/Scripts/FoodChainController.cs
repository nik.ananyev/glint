using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Varwin;
using Varwin.SocketLibrary;

public class FoodChainController : MonoBehaviour
{
    private SocketPoint[] socketPoints;
    //private FoodChainHandler foodChainHandler;
    public List<string> foodBlocks;
    public Image backgroundImage;
    [VarwinInspector(English: "Food block", Russian: "������� �����")]
    public List<string> StingArray
    {
        get => foodBlocks;
        set => foodBlocks = value;
    }
    public bool isComplete;
    [Variable(English: "Is Complete", Russian: "������� ���������")]
    public bool IsComplete
    {
        get => isComplete;
        set => isComplete = value;
    }
    /*public Color backgroundcolor;
    [Variable(English: "Background color", Russian: "���� ����")]
    public Color backgroundColor
    {
        get => backgroundcolor;
        set => backgroundcolor = value;
    }*/
    public delegate void OnCompleteEvent();
    [LogicEvent(English: "On complete", Russian: "������� ������� �������")]
    public event OnCompleteEvent onCompleteEvent;
    private void Start()
    {
        //backgroundImage.color = backgroundcolor;
        //foodChainHandler = FindObjectOfType<FoodChainHandler>();
        socketPoints = gameObject.GetComponentsInChildren<SocketPoint>();
        int i = 0;
        foreach(SocketPoint socketPoint in socketPoints)
        {
            socketPoint.AvailableKeys[0] = foodBlocks[i];
            socketPoint.OnConnect += CheckComplete;
            i += 1;
        }
    }
    public void CheckComplete(PlugPoint plugPoint, SocketPoint thisSocketPoint)
    {
        int i = 0;
        foreach (SocketPoint socketPoint in socketPoints)
        {
            if(!socketPoint.IsFree)
            {
                i += 1;
            }
        }
        if (i == socketPoints.Length)
        {
            isComplete = true;
            onCompleteEvent?.Invoke();
            //foodChainHandler.CheckAllComplete();
        }
    }
}
