using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Varwin;
public class FoodChainHandler : MonoBehaviour
{
    private FoodChainController[] foodChainControllers;
    public delegate void OnAllComplete();

    [LogicEvent(English: "On complete all chains", Russian: "������� ��� ������� �������")]
    public event OnAllComplete onAllCompleteEvent;

    private void Start()
    {
        foodChainControllers = FindObjectsOfType<FoodChainController>();
    }
    public void CheckAllComplete()
    {
        int i = 0;
        foreach(FoodChainController foodChainController in foodChainControllers)
        {
            if (foodChainController.IsComplete)
                i += 1;
        }
        if (i == foodChainControllers.Length)
            onAllCompleteEvent?.Invoke();
    }
}
