using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Varwin;
using Varwin.SocketLibrary;

public class FoodBlock : MonoBehaviour
{
    private PlugPoint plugPoint;
    public Image image;

    [VarwinInspector(English: "Key", Russian: "����")]
    public string key { get; set; }

    [VarwinInspector(English: "Img", Russian: "�����������")]
    public Sprite sprite { get; set; }
    private void Start()
    {
        plugPoint = gameObject.GetComponentInChildren<PlugPoint>();
        plugPoint.Key = key;
        image.sprite = sprite;
    }
}
