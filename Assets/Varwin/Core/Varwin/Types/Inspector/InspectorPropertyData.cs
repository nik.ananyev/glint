using System.Collections.Generic;
using System.Linq;

namespace Varwin
{
    public class InspectorPropertyData
    {
        public string ComponentPropertyName { get; set; }
        public PropertyValue PropertyValue { get; set; }
    }

    public class PropertyValue
    {
        public string ResourceGuid { get; set; }

        public object Value;

        public List<string> ResourceGuids { get; set; }

        public object GetPropertyRealValue(int index = -1)
        {
            if (Value != null)
            {
                return Value;
            }

            if (index != -1)
            {
                return index >= ResourceGuids.Count ? null : ResourceGuids[index];
            }

            return ResourceGuid;
        }

        public string GetPropertyValueString() => Value != null ? Value.ToString() : ResourceGuid;
    }

    public class InspectorPropertyInfo
    {
        public int ControllerId { get; }
        public ObjectController Controller { get; }
        public InspectorProperty Property { get; }

        public object PreModifyValue { get; set; }

        public object PreviousValue { get; set; }

        public InspectorPropertyInfo(ObjectController controller, InspectorProperty property)
        {
            ControllerId = controller.Id;
            Controller = controller;
            Property = property;
        }
    }
}