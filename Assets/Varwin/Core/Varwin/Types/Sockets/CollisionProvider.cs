﻿using System.Collections.Generic;
using UnityEngine;
using Varwin.ObjectsInteractions;
using Varwin.Public;

namespace Varwin.SocketLibrary
{
    /// <summary>
    /// Костыль, нужен для обхода ограничений CollisionController'a.
    /// </summary>
    public class CollisionProvider : ChainedJointControllerBase
    {
        /// <summary>
        /// Главный контроллер.
        /// </summary>
        public SocketController MainSocketController { get; internal set; }

        /// <summary>
        /// Обработчик коллизий.
        /// </summary>
        private CollisionController _collisionController;

        /// <summary>
        /// Обработчик коллизий.
        /// </summary>
        public CollisionController CollisionController => GetCollisionController();

        /// <summary>
        /// Имеется ли CollisionController.
        /// </summary>
        public bool HasCollisionController { get; private set; }

        /// <summary>
        /// Костыль для убирания дребезжания.
        /// </summary>
        private HashSet<Collider> _frameIgnored = new HashSet<Collider>();

        /// <summary>
        /// При взятии в руку.
        /// </summary>
        public void OnGrabStart()
        {
            CollisionController.enabled = true;
        }

        /// <summary>
        /// При отпускании.
        /// </summary>
        public void OnGrabEnd()
        {
            if (_collisionController)
            {
                CollisionController.Destroy();
            }
        }

        /// <summary>
        /// Получение контроллера коллизий.
        /// </summary>
        /// <returns>Обработчик коллизий.</returns>
        public CollisionController GetCollisionController()
        {
            if (!_collisionController)
            {
                _collisionController = GetComponentInParent<CollisionController>();
            }

            if (!_collisionController || _collisionController.IsDestroying)
            {
                _collisionController = gameObject.GetWrapper().GetGameObject().AddComponent<CollisionController>();
                var inputController = MainSocketController.gameObject.GetRootInputController();
                _collisionController.InitializeController(inputController);
            }

            return _collisionController;
        }

        /// <summary>
        /// При соприкосновении CollisionController'a.
        /// </summary>
        /// <param name="other">Другой коллайдер.</param>
        public override void CollisionEnter(Collider other)
        {
            if (_frameIgnored.Contains(other))
            {
                return;
            }

            if (MainSocketController.ConnectionGraphBehaviour.HasChild(other.GetComponentInParent<SocketController>()))
            {
                return;
            }

            _frameIgnored.Add(other);

            MainSocketController.ConnectionGraphBehaviour.ForEach(a => a.CollisionProvider.OnCollisionEnterInvoke(other));
        }

        /// <summary>
        /// При выходе из коллизии CollisionController'a.
        /// </summary>
        /// <param name="other">Другой коллайдер.</param>
        public override void CollisionExit(Collider other)
        {
            if (_frameIgnored.Contains(other))
            {
                return;
            }

            _frameIgnored.Remove(other);

            if (_frameIgnored.Count != 0)
            {
                return;
            }

            MainSocketController.ConnectionGraphBehaviour.ForEach(a => a.CollisionProvider.OnCollisionExitInvoke(other));
        }

        /// <summary>
        /// Разрешить отпустить из руки.
        /// </summary>
        public void CheckCanDrop()
        {
            if (!_collisionController || _collisionController.InputController == null || !CollisionController.IsBlocked() || !MainSocketController.IsLocalGrabbed)
            {
                return;
            }

            if (!_collisionController.InputController.IsDropEnabled() && ProjectData.PlatformMode == PlatformMode.Vr)
            {
                _collisionController.InputController.EnableDrop();
            }
            else
            {
                _collisionController.ForcedUnblock();
            }
        }

        /// <summary>
        /// Обновление флага.
        /// </summary>
        private void Update()
        {
            bool hasFlag = GetComponentInParent<CollisionController>();
            if (hasFlag == HasCollisionController)
            {
                return;
            }

            HasCollisionController = hasFlag;
            if (HasCollisionController)
            {
                OnInitCollisionController();
            }
        }

        /// <summary>
        /// При появлении CollisionController'a дать события.
        /// </summary>
        private void OnInitCollisionController()
        {
            CollisionController.SubscribeToJointControllerEvents(this);
        }

        /// <summary>
        /// Зачистка костыля.
        /// </summary>
        private void FixedUpdate()
        {
            _frameIgnored.Clear();
        }
    }
}