﻿using UnityEngine;
using Varwin.Public;

namespace Varwin
{
    /// <summary>
    /// Компонент, описывающий клона.
    /// </summary>
    public class CloneTarget : MonoBehaviour
    {
        /// <summary>
        /// Исходный компонент клонирования.
        /// </summary>
        private CloneSource _source;
        
        /// <summary>
        /// Список компонентов клона.
        /// </summary>
        private ICloneTarget[] _cloneTargets;

        /// <summary>
        /// Инициализация клона.
        /// </summary>
        /// <param name="source">Оригинальный объект.</param>
        public void Initialize(CloneSource source)
        {
            _source = source;
            _cloneTargets = GetComponentsInChildren<ICloneTarget>(true);

            foreach (var cloneTarget in _cloneTargets)
            {
                cloneTarget.OnInitialize(_source.gameObject.GetWrapper());
            }
        }

        /// <summary>
        /// Удаление клона из списка клонов.
        /// </summary>
        private void OnDestroy()
        {
            _source.DestroyClone(gameObject);
        }
    }
}