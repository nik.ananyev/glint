﻿using System;
using UnityEngine;
using Varwin.Log.Interfaces;

namespace Varwin.Log
{
    /// <summary>
    /// Класс для создание различных логгеров.
    /// </summary>
    public static class VarwinLoggerFactory
    {
        /// <summary>
        /// Имя параметра запуска с путем до файла логов.
        /// </summary>
        private const string LogPathLaunchArg = "-logFilePath";

        /// <summary>
        /// Текущий логер.
        /// </summary>
        private static IVarwinLogger CurrentLogger { get; set; }

        /// <summary>
        /// Инициализация логера при загрузке проекта.
        /// </summary>
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void Initialization()
        {
            //TODO: Сделать логгер для мобильных клиентов.
#if UNITY_ANDROID
            //MobileLoggerSettings.Init();
#else
            var logFilePath = string.Empty;
            string[] args = Environment.GetCommandLineArgs();
            try
            {
                for (var i = 0; i < args.Length; i++)
                {
                    if(args[i].Contains(LogPathLaunchArg))
                    {
                        logFilePath = args[i + 1];
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError($"Error with read launch args: {string.Join(", ", args)}\n{e}");
                throw;
            }

            SetupLog(new DesktopClientLogger(logFilePath));
#endif
        }

        /// <summary>
        /// Инициализация экземпляра логгера.
        /// </summary>
        /// <param name="logger"></param>
        private static void SetupLog(IVarwinLogger logger)
        {
            CurrentLogger?.Dispose();

            CurrentLogger = logger;
            Application.logMessageReceivedThreaded += CurrentLogger.OnLogMessageReceived;
            CoreErrorManager.OnError += CurrentLogger.HandleCoreException;
            Application.quitting += () => CurrentLogger.Dispose();
        }
    }
}