﻿using System;
using UnityEngine;

namespace Varwin.Log.Interfaces
{
    /// <summary>
    /// Интерфейс для описания логера.
    /// </summary>
    public interface IVarwinLogger : IDisposable
    {
        /// <summary>
        /// Коллебек на получение сообщения об ошибке.
        /// </summary>
        /// <param name="condition">Сообщение об ошибке.</param>
        /// <param name="stackTrace">Стек вызовов.</param>
        /// <param name="logType">Тип ошибки.</param>
        void OnLogMessageReceived(string condition, string stackTrace, LogType logType);

        /// <summary>
        /// Обработка ошибок из ядра.
        /// </summary>
        /// <param name="exception">Исключение.</param>
        void HandleCoreException(Exception exception);
    }
}