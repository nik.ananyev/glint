﻿using System;
using Varwin.Log.Messages;

namespace Varwin.Log.Interfaces
{
    /// <summary>
    /// Интерфейс для целей логирования.
    /// </summary>
    public interface ILogTarget : IDisposable
    {
        /// <summary>
        /// Выполнить логирование.
        /// </summary>
        /// <param name="logMessage">Сообщение для логирования.</param>
        void Log(BaseLogMessage logMessage);
    }
}