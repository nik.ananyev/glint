﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Varwin.Log.Interfaces;
using Varwin.Log.Messages;

namespace Varwin.Log
{
    /// <summary>
    /// Логгер для десктопных клиентов.
    /// </summary>
    public class DesktopClientLogger : IVarwinLogger
    {

        /// <summary>
        /// Путь для складывания логов, если клиент запущен в Unity.
        /// </summary>
        private const string UnityEditorLogPath = "VarwinData/Logs/client/";

        /// <summary>
        /// Имя файла для складывания логов, если клиент запущен в Unity.
        /// </summary>
        private const string LogFileName = "client.log";

        /// <summary>
        /// Список целей для логирования.
        /// </summary>
        private readonly List<ILogTarget> _logTargets = new();

        /// <summary>
        /// Путь до файла логов.
        /// </summary>
        private string _logPath;

        /// <summary>
        /// Создает экземпляр логгера для десктопа.
        /// </summary>
        /// <param name="logFilePath">Путь до файла логов.</param>
        public DesktopClientLogger(string logFilePath)
        {
            if (Application.isEditor)
            {
                CreateDefaultLogFile();
            }
            else
            {
                if (string.IsNullOrEmpty(logFilePath))
                {
                    CreateDefaultLogFile();
                }
                else
                {
                    _logPath = logFilePath;
                    DeleteFileIfExists(_logPath);
                }   
            }

            _logTargets.Add(new FileLogTarget(_logPath));

            Application.quitting += Dispose;
        }

        /// <summary>
        /// <inheritdoc cref="IVarwinLogger.OnLogMessageReceived"/>
        /// </summary>
        public void OnLogMessageReceived(string condition, string stackTrace, LogType logType)
        {
            var logMessage = new LogMessage(condition, stackTrace, DateTime.Now, logType,
                showStackTrace: logType is LogType.Error or LogType.Exception);

            _logTargets.ForEach(logTarget =>
            {
                logTarget.Log(logMessage);
            });
        }

        /// <summary>
        /// <inheritdoc cref="IVarwinLogger.HandleCoreException"/>
        /// </summary>
        public void HandleCoreException(Exception exception)
        {
            var message = new LogMessage(exception.Message, exception.StackTrace, DateTime.Now, LogType.Exception, true);

            _logTargets.ForEach(logTarget =>
            {
                logTarget.Log(message);
            });
        }

        /// <summary>
        /// Очищение ресурсов для всех целей логирования.
        /// </summary>
        public void Dispose()
        {
            Application.quitting -= Dispose;

            _logTargets.ForEach(logTarget =>
            {
                logTarget.Dispose();
            });
        }

        /// <summary>
        /// Создание дефлотного лога в системной директории.
        /// </summary>
        private void CreateDefaultLogFile()
        {
            try
            {
                var logDirectory = Path.Combine(Path.GetPathRoot(Environment.SystemDirectory), UnityEditorLogPath);

                if (!Directory.Exists(logDirectory))
                {
                    Directory.CreateDirectory(logDirectory);
                }

                _logPath = Path.Combine(logDirectory, LogFileName);

                DeleteFileIfExists(_logPath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void DeleteFileIfExists(string filePath)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
    }
}