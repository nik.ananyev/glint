﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using Varwin.Log.Interfaces;
using Varwin.Log.Messages;

namespace Varwin.Log
{
    /// <summary>
    /// Класс для описание логирования в файл.
    /// </summary>
    public class FileLogTarget : ILogTarget
    {
        /// <summary>
        /// Путь до файла для записи.
        /// </summary>
        private readonly string _filePath;

        /// <summary>
        /// Поток записи в файл.
        /// </summary>
        private StreamWriter _writeFileStream;

        /// <summary>
        /// Список сообщений для логирования.
        /// </summary>
        private ConcurrentQueue<BaseLogMessage> _logMessages;

        /// <summary>
        /// Рабочий поток для записи логов в файл.
        /// </summary>
        private Thread _workingThread;

        /// <summary>
        /// Флаг для потока записи. Присваивается значение true при очистке ресурсов лог таргета.
        /// </summary>
        private bool _disposing;

        /// <summary>
        /// Событие о записи в очередь для потока. Используется для оптимизации потока записи.
        /// </summary>
        private ManualResetEvent _threadResetEvent = new ManualResetEvent(true);

        /// <summary>
        /// Инициализация класса записи логов в файлю
        /// </summary>
        /// <param name="logFilePath">Путь до файла, в который будет производиться запсиь.</param>
        /// <param name="numberLogsToWrite">Количество сообщений, необходимое для записи в файл.</param>
        public FileLogTarget(string logFilePath)
        {
            _filePath = logFilePath;

            _logMessages = new();
            _writeFileStream = File.AppendText(_filePath);

            _workingThread = new Thread(StoreMessages)
            {
                IsBackground = true,
                Priority = ThreadPriority.BelowNormal
            };
            _workingThread.Start();
        }

        /// <summary>
        /// <inheritdoc cref="ILogTarget.Log"/>
        /// </summary>
        /// <param name="logMessage">Сообщение для логирования.</param>
        public void Log(BaseLogMessage logMessage)
        {
            _logMessages.Enqueue(logMessage);
            _threadResetEvent.Set();
        }

        /// <summary>
        /// Записиь всех сообщений в файл.
        /// </summary>
        private void WriteToFile(BaseLogMessage logMessage)
        {
            _writeFileStream.WriteLine(logMessage.ToString());
        }

        /// <summary>
        /// Функция, выполняющаяся в потоке. Выполняет запись в файл, если есть сообщения для записи.
        /// </summary>
        private void StoreMessages()
        {
            while (!_disposing)
            {
                while (!_logMessages.IsEmpty)
                {
                    try
                    {
                        if (!_logMessages.TryPeek(out var message))
                        {
                            Thread.Sleep(5);
                        }

                        if (_logMessages.TryDequeue(out message))
                        {
                            WriteToFile(message);
                        }
                        else
                        {
                            Thread.Sleep(5);
                        }
                    }
                    catch (Exception e)
                    {
                        break;
                    }
                }

                _threadResetEvent.Reset();
                _threadResetEvent.WaitOne(500);
            }
        }

        
        /// <summary>
        /// Запись оставшихся сообщений в файл, закрытие потока записи в файл.
        /// </summary>
        public void Dispose()
        {
            _disposing = true;
            _logMessages?.Clear();
            _writeFileStream?.Dispose();
            _workingThread?.Abort();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Очищаем ресурсы, если при вызове деструктора они еще не очищены.
        /// </summary>
        ~FileLogTarget()
        {
            if (!_disposing)
            {
                Dispose();
            }
        }
    }
}