﻿using System;
using UnityEngine;

namespace Varwin.Log.Messages
{
    /// <summary>
    /// Класс стандартного сообщения.
    /// </summary>
    public class LogMessage : BaseLogMessage
    {
        /// <summary>
        /// Формат даты-времени для записи в лог.
        /// </summary>
        private const string DateTimeFormat = "yyyy-MM-dd hh:mm:ss";

        public LogMessage(string message, string stackTrace, DateTime logTime, LogType logType, bool showStackTrace = false) 
            : base(message, stackTrace, logTime, logType, showStackTrace)
        {}

        /// <summary>
        /// <inheritdoc cref="BaseLogMessage.ToString"/>
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var message = ShowStackTrace
                ? $"{LogTime.ToString(DateTimeFormat)} {LogTypeToString[LogType]} {Message} {Environment.NewLine} " +
                  $"STACKTRACE: {Environment.NewLine}{StackTrace}"
                : $"{LogTime.ToString(DateTimeFormat)} {LogTypeToString[LogType]} {Message}";

            return message;
        }
    }
}