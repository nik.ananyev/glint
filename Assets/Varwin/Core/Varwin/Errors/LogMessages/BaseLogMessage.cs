﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Varwin.Log.Messages
{
    /// <summary>
    /// Базовый класс для всех сообщений логирования.
    /// </summary>
    public abstract class BaseLogMessage
    {
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public DateTime LogTime { get; set; }
        public LogType LogType { get; set; }
        public bool ShowStackTrace { get; set; }

        /// <summary>
        /// Словарь для перевода энама ошибки в строку.
        /// </summary>
        protected static readonly Dictionary<LogType, string> LogTypeToString = new()
        {
            {LogType.Log, "INFO"},
            {LogType.Warning, "WARN"},
            {LogType.Error, "ERROR"},
            {LogType.Assert, "ERROR"},
            {LogType.Exception, "EXCEPTION"},
        };

        /// <summary>
        /// Создание экземпляра сообщения для логирования.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        /// <param name="stackTrace">Стек вызовов.</param>
        /// <param name="logTime">Время появления сообщения.</param>
        /// <param name="logType">Тип сообщения.</param>
        /// <param name="showStackTrace">Нужно ли записывать стек вызовов.</param>
        public BaseLogMessage(string message, string stackTrace, DateTime logTime, LogType logType, bool showStackTrace)
        {
            Message = message;
            StackTrace = stackTrace;
            LogTime = logTime;
            LogType = logType;
            ShowStackTrace = showStackTrace;
        }

        /// <summary>
        /// Метод для приведения сообщения в нужном формате к строке.
        /// </summary>
        /// <returns></returns>
        public abstract override string ToString();
    }
}