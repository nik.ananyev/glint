using UnityEngine;

namespace Varwin.Core
{
    public static class SystemLanguageEx
    {
        public static string GetCode(this SystemLanguage systemLanguage)
        {
            switch (systemLanguage)
            {
                case SystemLanguage.Chinese:
                    return "cn";
                case SystemLanguage.ChineseSimplified:
                    return "cn_s";
                case SystemLanguage.ChineseTraditional:
                    return "cn_t";
                case SystemLanguage.Japanese:
                    return "jp";
                case SystemLanguage.Portuguese:
                    return "pg";
                case SystemLanguage.Slovak:
                    return "sk";
                case SystemLanguage.Slovenian:
                    return "sv";
                case SystemLanguage.Indonesian:
                    return "ind";
                default:
                    return systemLanguage.ToString().Substring(0, 2).ToLowerInvariant();
            }
        }
    }
}