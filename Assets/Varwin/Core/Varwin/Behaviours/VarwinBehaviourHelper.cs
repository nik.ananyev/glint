﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Varwin.Public;

namespace Varwin.Core.Behaviours
{
    public class VarwinBehaviourHelper
    {
        public virtual bool CanAddBehaviour(GameObject gameObject, Type behaviourType)
        {
            if (IsDisabledBehaviour(gameObject))
            {
                return false;
            }
            
            IEnumerable<Type> requiredComponentTypes = Attribute.GetCustomAttributes(behaviourType, typeof(RequireComponentInChildrenAttribute))
                .OfType<RequireComponentInChildrenAttribute>().Select(x => x.RequiredComponent);

            var protectedComponentTypes = new List<Type>();

            foreach (var objectBehaviour in gameObject.GetComponentsInChildren<MonoBehaviour>(true))
            {
                protectedComponentTypes.AddRange(Attribute.GetCustomAttributes(objectBehaviour.GetType(), typeof(ProtectComponentAttribute))
                    .OfType<ProtectComponentAttribute>().Select(x => x.ProtectedComponent));
            }
            
            foreach (var type in requiredComponentTypes)
            {
                if (gameObject.GetComponentsInChildren(type, true).Length == 0)
                {
                    return false;
                }
                
                if (protectedComponentTypes.Contains(type))
                {
                    return false;
                }
            }

            if (gameObject.GetComponentInChildren<VarwinBot>())
            {
                return false;
            }

            return !gameObject.GetComponent(behaviourType);
        }

        public virtual bool IsDisabledBehaviour(GameObject targetGameObject)
        {
            return false;
        }

        protected bool IsDisabledBehaviour(GameObject targetGameObject, BehaviourType behaviourType)
        {
            foreach (var objectBehaviour in targetGameObject.GetComponentsInChildren<MonoBehaviour>())
            {
                var disabledBehaviourAttributes = objectBehaviour.GetType().GetCustomAttributes(typeof(DisableDefaultBehaviourAttribute), true);

                foreach (DisableDefaultBehaviourAttribute disableDefaultBehaviourAttribute in disabledBehaviourAttributes)
                {
                    if (disableDefaultBehaviourAttribute != null && disableDefaultBehaviourAttribute.BehaviourType == behaviourType)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
