﻿using System;
using UnityEngine;

#if !VARWIN_SDK
#if UNITY_STANDALONE
namespace ZenFulcrum.EmbeddedBrowser
{
    public class VRBrowserPanel : MonoBehaviour, INewWindowHandler
    {
        public Browser contentBrowser, controlBrowser;

        public Transform keyboardLocation;

        public bool loaded;
        public bool closeBrowser;

        public void Awake()
        {
#if !VRMAKER
            //If the content browser is externally closed, make sure we go too.
            var dd = contentBrowser.gameObject.AddComponent<DestroyDetector>();
            dd.onDestroy += CloseBrowser;

            contentBrowser.SetNewWindowHandler(Browser.NewWindowAction.NewBrowser, this);
            contentBrowser.onLoad += data => controlBrowser.CallFunction("setURL", data["url"]);

            VRMainControlPanel.instance.keyboard.onFocusChange += OnKeyboardOnOnFocusChange;

            contentBrowser.onLoad += ContentBrowserOnOnLoad;
#endif
        }

        private void ContentBrowserOnOnLoad(JSONNode jsonNode)
        {
            loaded = true;
        }

        public void OnDestroy()
        {
#if !VRMAKER
            VRMainControlPanel.instance.keyboard.onFocusChange -= OnKeyboardOnOnFocusChange;
#endif
        }

        private void OnKeyboardOnOnFocusChange(Browser browser, bool editable)
        {
#if !VRMAKER
            if (!editable || !browser) VRMainControlPanel.instance.MoveKeyboardUnder(null);
            else if (browser == contentBrowser || browser == controlBrowser) VRMainControlPanel.instance.MoveKeyboardUnder(this);
#endif
        }

        public void CloseBrowser()
        {
#if !VRMAKER
            if (!this || !VRMainControlPanel.instance) return;

            closeBrowser = true;
#endif
        }

        public Browser CreateBrowser(Browser parent)
        {
#if !VRMAKER
            var newPane = VRMainControlPanel.instance.OpenNewTab(this);
            newPane.transform.position = transform.position;
            newPane.transform.rotation = transform.rotation;
            return newPane.contentBrowser;
#endif
            return null;
        }
    }

    internal class DestroyDetector : MonoBehaviour
    {
        public event Action onDestroy = () => { };

        public void OnDestroy()
        {
            onDestroy();
        }
    }
}
#endif
#endif