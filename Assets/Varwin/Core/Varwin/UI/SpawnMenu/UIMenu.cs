﻿using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
using UnityEngine;
using Varwin.Data;
using Varwin.Data.ServerData;
using Varwin.Models.Data;
using Varwin.UI.Varwin.UI;
using Varwin.PlatformAdapter;
#if VARWINCLIENT && !PLATFORM_ANDROID
using Varwin.WWW;
using ZenFulcrum.EmbeddedBrowser;
#endif

namespace Varwin.UI
{
    // ReSharper disable once InconsistentNaming
    public class UIMenu : MonoBehaviour
    {
        public GameObject BrowserWindow;
        public GameObject SpawnSphere;
        public GameObject ModeButton;
        public GameObject ModeButtonOffset;
        public bool IsReady;
#if VARWINCLIENT && !PLATFORM_ANDROID
        public Browser Browser;
#endif
        public static UIMenu Instance;

        private bool _htmlLoaded;
        private bool _isLoading = true;
        private bool _openAfterLoad;
        private TransformDT _savedTransform;
        private const float ReloadTime = 5f;
        private float _updateLoadTime = ReloadTime;
        private Transform _pointerOrigin;

        private bool _isOpened;

        public static bool IsOpened => Instance && Instance._isOpened;

        public delegate void MenuOpenHandler();

        public delegate void MenuCloseHandler();

        public event MenuOpenHandler OnMenuOpened;
        public event MenuCloseHandler OnMenuClosed;

        private void Awake()
        {
            Instance = this;
            ModeButton.SetActive(false);
            _savedTransform = BrowserWindow.transform.ToLocalTransformDT();
            ModeButtonOffset = new GameObject("Move Button Pivot");
            ModeButtonOffset.transform.SetParent(transform);
            ModeButtonOffset.transform.localRotation = Quaternion.identity;
            ModeButtonOffset.transform.localPosition = new Vector3(-0.5f, -2.29f, 17.75f);
            StartCoroutine(WaitForVRBrowserPanel());
        }

        private void Update()
        {
            if (!IsReady)
            {
                return;
            }

            if (!_isLoading)
            {
                return;
            }

            if (_updateLoadTime > 0)
            {
                _updateLoadTime -= Time.deltaTime;
            }
            else
            {
                _updateLoadTime = ReloadTime;
                LoadMenu();
            }
        }

#if VARWINCLIENT && !PLATFORM_ANDROID
        public void InitBrowser(Browser browser)
        {
            Browser = browser;
        }

        private void ShowHtml()
        {
            var accessToken = "";

            if (!string.IsNullOrEmpty(Request.AccessToken))
            {
                accessToken = "&access_token=" + Request.AccessToken;
            }

            string uri = Settings.Instance.WebHost
                         + "/mypad?lang=" + Settings.Instance.Language
                         + "&scene_id=" + ProjectData.SceneId
                         + accessToken
                         + "&workspace_id=" + Request.WorkspaceId;

            Browser.LoadURL(uri, true);

            Browser.Resize(1536, 1600);

            Browser.RegisterFunction("spawnObject",
                args =>
                {
                    string objectString = args[0].AsJSON;

                    PrefabObject prefabObject = objectString.JsonDeserialize<PrefabObject>();

                    SceneObjectDto existedSceneObject = ProjectData.ProjectStructure.Scenes.GetProjectScene(ProjectData.SceneId)
                        .SceneObjects.Find(p => p.ObjectId == prefabObject.Id);


                    Debug.Log("Object to spawn: " + prefabObject.Guid);
                    int objectId = prefabObject.Id;

                    if (GameStateData.GetPrefabGameObject(objectId))
                    {
                        SpawnManager.SetSpawnedObject(objectId);
                        Debug.Log("Spawn existed object: " + objectId);
                    }
                    else if (existedSceneObject != null)
                    {
                        Debug.Log("Object " + prefabObject.Guid + " is already loading!");
                        HideMenu();

                        return;
                    }
                    else
                    {
                        ProjectData.ProjectStructure.Objects.Add(prefabObject);
                        var po = new List<PrefabObject> {prefabObject};


                        void OnLoadObjects()
                        {
                            if (GameStateData.GetPrefabGameObject(objectId) != null && !BrowserWindow.activeSelf)
                            {
                                SpawnManager.SetSpawnedObject(objectId);
                                Debug.Log("New object spawned: " + objectId);
                            }
                            
                            ProjectData.ObjectsLoaded -= OnLoadObjects;
                        }


                        LoaderAdapter.LoadPrefabObjects(po);
                        ProjectData.ObjectsLoaded += OnLoadObjects;
                    }

                    HideMenu();
                });
            
            Browser.RegisterFunction("switchMode", args => { SwitchGameMode(); });
            Browser.RegisterFunction("save", args => { Helper.SaveSceneObjects(); });

            SetRender();
            _htmlLoaded = true;
        }
#endif

        private void SetRender()
        {
            var renders = GetComponentsInChildren<Renderer>();

            foreach (Renderer render in renders)
            {
                render.material.renderQueue = 3110;
            }
        }

        private void ApplyGameMode()
        {
            GameMode oldGm = ProjectData.GameMode;
            ProjectData.GameMode = ProjectData.GameMode == GameMode.Edit ? GameMode.Preview : GameMode.Edit;
            SpawnManager.ResetSpawnObject();

            if (oldGm == GameMode.Preview && ProjectData.GameMode == GameMode.Edit)
            {
                Helper.ReloadScene();
            }
        }

        private void SwitchGameMode()
        {
            BrowserWindow.SetActive(false);

            if (ProjectData.GameMode == GameMode.View)
            {
                return;
            }

            if (ProjectData.GameMode == GameMode.Edit && ProjectData.ObjectsAreChanged)
            {
                Helper.AskUserToDo(LanguageManager.Instance.GetTextValue("GROUP_NOT_SAVED"),
                    () =>
                    {
                        Helper.SaveSceneObjects();
                        ApplyGameMode();
                    },
                    () =>
                    {
                        Helper.ReloadScene();
                        ApplyGameMode();
                    },
                    HideMenu);
            }
            else
            {
                ApplyGameMode();
            }
        }

        private void LoadMenu()
        {
            if (_htmlLoaded)
            {
                Debug.Log("Menu is alredy loaded");

                return;
            }

            Debug.Log("Load menu started");
            _isLoading = true;
            BrowserWindow.transform.position = new Vector3(10000, 100000, 0);
            BrowserWindow.SetActive(true);

            if (!_htmlLoaded)
            {
#if VARWINCLIENT && !PLATFORM_ANDROID
                ShowHtml();
#endif
            }
        }

        public void ShowMenu()
        {
            if (_isLoading && ProjectData.GameMode != GameMode.View)
            {
                _openAfterLoad = true;

                return;
            }

            if (VRMessageManager.VRMessageManager.Instance.IsShowing)
            {
                return;
            }

            if (ProjectData.GameMode != GameMode.View && ProjectData.GameMode != GameMode.Preview)
            {
                if (!_pointerOrigin)
                {
                    UpdatePointerOrigns();

                    if (!_pointerOrigin)
                    {
                        return;
                    }
                }

#if VARWINCLIENT && !PLATFORM_ANDROID
                PointerUIBase.RightHand = _pointerOrigin;
#endif

                SpawnSphere.SetActive(true);
                ModeButton.SetActive(false);
                BrowserWindow.SetActive(true);
                SpawnManager.ResetSpawnObject();

                OnMenuOpened?.Invoke();
            }
            else
            {
                SpawnSphere.SetActive(false);

                ModeButton.SetActive(ProjectData.GameMode != GameMode.View);

                BrowserWindow.SetActive(false);
            }

            _isOpened = true;
        }

        private void UpdatePointerOrigns()
        {
            var rightHand = InputAdapter.Instance.PlayerController.Nodes.RightHand;

            Transform origins = rightHand.Transform.Find("PointerOrigins");

            if (!origins)
            {
                _pointerOrigin = rightHand.Transform;

                return;
            }

            if (DeviceHelper.IsOculus)
            {
                _pointerOrigin = origins.Find("Oculus");
            }
            else
            {
                _pointerOrigin = origins.Find("Generic");
            }

            if (!_pointerOrigin)
            {
                _pointerOrigin = rightHand.Transform;
            }
        }

        public void BrowserLoaded()
        {
            _isLoading = false;
            _savedTransform.ToLocalTransformUnity(BrowserWindow.transform);
            BrowserWindow.SetActive(false);
            Debug.Log("Browser is loaded");

            BrowserWindow.transform.GetChild(0).GetChild(0).gameObject.AddComponent<UIPanel>();

            if (_openAfterLoad)
            {
                ShowMenu();
            }
        }

        public void HideMenu()
        {
            if (_isLoading && ProjectData.GameMode != GameMode.View)
            {
                _openAfterLoad = false;
                return;
            }

            BrowserWindow.SetActive(false);

            OnMenuClosed?.Invoke();
            _isOpened = false;
            ModeButton.SetActive(false);
        }

        private IEnumerator WaitForVRBrowserPanel()
        {
#if VARWINCLIENT && !PLATFORM_ANDROID
            var vrBrowserPanel = FindObjectOfType<VRBrowserPanel>();

            while (!vrBrowserPanel)
            {
                BrowserWindow.SetActive(true);
                vrBrowserPanel = FindObjectOfType<VRBrowserPanel>();
                BrowserWindow.SetActive(false);
                yield return null;
            }

            BrowserWindow.SetActive(true);
            InitBrowser(vrBrowserPanel.contentBrowser);

            yield return null;
            LoadMenu();

            while (!vrBrowserPanel.loaded)
            {
                yield return null;
            }

            yield return null;

            BrowserLoaded();

            IsReady = true;
#endif
            yield return true;
        }
    }
}