using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;
using Varwin.TextToSpeech;

namespace Varwin.Public
{
    [RequireComponent(
        typeof(VarwinObjectDescriptor), 
        typeof(Animator), 
        typeof(CharacterController))]
    [DisallowMultipleComponent]
    [VarwinComponent(English:"Bot",Russian:"Бот",Chinese:"機器人",Korean:"봇")]
    public class VarwinBot : MonoBehaviour, ISwitchModeSubscriber
    {
        public delegate void BotTargetReachedEventHandler([Parameter(English:"target object",Russian:"целевой объект",Chinese:"目標物件",Korean:"목표 객체")] Wrapper target);
        [LogicEvent(English:"Target object reached",Russian:"Целевой объект достигнут",Chinese:"抵達目標物件",Korean:"대상 객체에 도달했을 때")]
        [EventCustomSender(English:"Bot",Russian:"Бот",Chinese:"機器人",Korean:"봇")]
        public event BotTargetReachedEventHandler BotTargetReached;

        public delegate void BotPathPointEventHandler(
            [Parameter(English:"waypoint number",Russian:"номер точки в маршруте",Chinese:"導航點編號",Korean:"웨이포인트 번호")] int id, 
            [Parameter(English:"waypoint number",Russian:"номер точки в маршруте",Chinese:"導航點編號",Korean:"웨이포인트 번호")] Wrapper point);
        
        [LogicEvent(English:"Path point reached",Russian:"Точка пути достигнута",Chinese:"抵達路徑點",Korean:"경로 지점에 도달했을 때")]
        [EventCustomSender(English:"Bot",Russian:"Бот",Chinese:"機器人",Korean:"봇")]
        public event BotPathPointEventHandler BotPathPointReached;
        
        public enum MovementPace
        {
            [Item(English:"Walking",Russian:"Шагом",Chinese:"行走",Korean:"걷기")]
            Walk,
            [Item(English:"Running",Russian:"Бегом",Chinese:"奔跑",Korean:"달리기")]
            Run
        }
        
        public enum MovementDirection
        {
            [Item(English:"Forward",Russian:"Вперед",Chinese:"前進",Korean:"전방")]
            Forward,
            [Item(English:"Backward",Russian:"Назад",Chinese:"後退",Korean:"후방")]
            Backward,
            [Item(English:"Left",Russian:"Влево",Chinese:"左行",Korean:"왼쪽")]
            Left,
            [Item(English:"Right",Russian:"Вправо",Chinese:"右行",Korean:"오른쪽")]
            Right
        }
        
        public enum RotationDirection
        {
            [Item(English:"Clockwise",Russian:"По часовой стрелке",Chinese:"順時針",Korean:"시계방향")]
            Clockwise,
            [Item(English:"Counterclockwise",Russian:"Против часовой стрелки",Chinese:"逆時針",Korean:"시계반대방향")]
            Counterclockwise
        }
        
        public enum TextBubbleHideType
        {
            [Item(English:"Automatic",Russian:"Автоматически",Chinese:"自動",Korean:"자동")]
            Automatic,
            [Item(English:"Never",Russian:"Никогда",Chinese:"從不",Korean:"절대")]
            Never
        }
        
        [Obsolete]
        [HideInInspector]
        public List<VarwinBotCustomAnimation> CustomAnimations;
        
        public enum MovementType
        {
            None,
            Infinite,
            ForMeters,
            TowardsObject,
            SimplePath
        }

        private float _targetObjectMinDistance = 0.5f;
        private float _groundCheckMaxDistance = 2f;
        
        private Animator _animator;
        private Rigidbody _rigidbody;
        private CharacterController _characterController;

        public CharacterController CharacterController => _characterController;

        [Obsolete] private int _animationClipId;
        
        private GameObject _targetObject;
        private float _lastDistance;
        private int _currentPathPoint;
        private List<Transform> _pathPoints;
        private bool _pathActive;
        
        private MovementType _currentMovementType;
        private MovementPace _currentMovementPace;
        private MovementDirection _currentMovementDirection;

        public MovementPace CurrentMovementPace => _currentMovementPace;
        public MovementType CurrentMovementType => _currentMovementType;

        public MovementDirection CurrentMovementDirection => _currentMovementDirection;

        private float _distanceMoved;
        private float _distanceRequired;
        private Vector3 _lastPosition;
        
        private float _angularVelocity;
        private float _rotationTime;
        private float _currentRotationTime;
        
        [Obsolete] private bool _repeatAnimation;
        [Obsolete] private RuntimeAnimatorController _animatorController;
        
        [Obsolete] private PlayableGraph _playableGraph;
        [Obsolete] private AnimationPlayableOutput _animationPlayableOutput;
        [Obsolete] private List<AnimationClipPlayable> _clipPlayables;
        
        private VarwinBotTextBubble _textBubble;
        private VarwinBotTextToSpeech _textToSpeech;
        
        [SerializeField] private bool _useCustomAnimatorController = false;
        
        private bool _showTextBubble;
        
        [Obsolete]
        public bool IsHumanoid
        {
            get
            {
                if (!_animator)
                {
                    _animator = GetComponent<Animator>();
                }
                
                return _animator && _animator.avatar && _animator.avatar.isHuman;
            }
        }
        
        [ActionGroup("CharacterControllerParameters")]
        [Action(English:"Set max traversable slope angle",Russian:"Задать максимальный угол подъема",Chinese:"設定最大可行走坡度",Korean:"최대 등반 가능한 경사각 설정")]
        public void SetMaxTraversableSlope(float slope)
        {
            _characterController.slopeLimit = slope;
        }
        
        [ActionGroup("CharacterControllerParameters")]
        [Action(English:"Set max step height",Russian:"Задать максимальную высоту шага",Chinese:"設定最大步伐高度",Korean:"최대 발걸음 높이 설정")]
        public void SetMaxStepHeight(float stepHeight)
        {
            _characterController.stepOffset = Mathf.Clamp(stepHeight, 0, _characterController.height);;
        }        
        
        [Action(English:"Set object stop distance",Russian:"Задать расстояние остановки перед объектом",Chinese:"設定碰到物件停止的最小距離",Korean:"객체 정지 거리 설정")]
        public void SetMinObjectDistance(float objectDistance)
        {
            _targetObjectMinDistance = objectDistance;
        }
        
        [Action(English:"Rotate",Russian:"Повернуть",Chinese:"旋轉",Korean:"회전")]
        [ArgsFormat(English:"for {%} degrees {%} in {%} sec",Russian:"на {%} градусов {%} за {%} сек",Chinese:"對物件{%}，旋轉{%}度於{%}秒內",Korean:"{%} 초 동안 {%} (으)로  {%} 도 회전")]
        public void RotateForDegrees(float angle, RotationDirection rotationDirection, float time)
        {
            _angularVelocity = angle / time;

            if (rotationDirection == RotationDirection.Counterclockwise)
            {
                _angularVelocity *= -1;
            }

            _rotationTime = time;
            _currentRotationTime = 0;
        }
        
        [Action(English:"Move",Russian:"Двигаться",Chinese:"移動",Korean:"이동")]
        [ArgsFormat(English:"{%} at {%} pace until stopped",Russian:"{%} {%} до остановки",Chinese:"以 {%} 的速度 {%} 直到停止",Korean:"{%}(으)로 {%} 페이스를 유지하여 멈출 때 까지")]
        public void MoveInfinite(MovementDirection movementDirection, MovementPace movementPace)
        {
            MoveBot(movementDirection, movementPace);

            _currentMovementType = MovementType.Infinite;
        }
        
        [Action(English:"Move",Russian:"Передвинуться",Chinese:"移動",Korean:"이동")]
        [ArgsFormat(English:"{%} at {%} pace for {%} m",Russian:"{%} {%} на расстояние {%} м",Chinese:"{%} 以 {%} 步速持續 {%} 米",Korean:"{%}방향을 향해 {%} 페이스로 {%} 미터")]
        public void MoveForMeters(MovementDirection movementDirection, MovementPace movementPace, float distance)
        {
            float clampedDistance = Mathf.Clamp(distance, 0, distance);
            
            if (clampedDistance == 0) 
            {
                StopMovement();
                return;
            }
            
            _distanceMoved = 0;
            _distanceRequired = distance;
            
            _lastPosition = transform.position;
            
            MoveBot(movementDirection, movementPace);

            _currentMovementType = MovementType.ForMeters;
        }
        
        [Action(English:"Move",Russian:"Двигаться",Chinese:"移動",Korean:"이동")]
        [ArgsFormat(English:"at {%} pace towards object {%}",Russian:"{%} в сторону объекта {%}",Chinese:"以{%}的速度朝向物件{%}移動",Korean:"{%} 페이스로 {%} 객체를 향함")]
        public void MoveTowardsObject(MovementPace movementPace, Wrapper wrapper)
        {
            _targetObject = wrapper.GetGameObject();
            
            _currentMovementPace = movementPace;
            _currentMovementType = MovementType.TowardsObject;
            
           _lastDistance = 0;
           
           _animator.SetFloat("InputVertical", 1);
        }    
        
        
#if !NET_STANDARD_2_0
        [Action(English:"Move",Russian:"Двигаться",Chinese:"移動",Korean:"이동")]
        [ArgsFormat(English:"at {%} pace along the path {%}",Russian:"{%} по маршруту {%}",Chinese:"以{%}的速度沿著路徑{%}移動",Korean:"{%}페이스로 {%}경로를 따름")]
        public void MoveByPathSimple(MovementPace movementPace, dynamic points)
        {
            List<dynamic> pointsList;
            
            if (points is List<dynamic>)
            {
                pointsList = points;
            }
            else if(points is Wrapper)
            {
                pointsList = new List<dynamic> {points};
            }
            else
            {
                return;
            }

            _pathPoints = new List<Transform>();

            foreach (dynamic point in pointsList)
            {
                Wrapper wrapper = point as Wrapper;

                if (wrapper != null)
                {
                    _pathPoints.Add(point.GetGameObject().transform);
                }
            }

            if (_pathPoints.Count == 0)
            {
                StopMovement();

                return;
            }
            
            _currentMovementPace = movementPace;
            _currentMovementType = MovementType.SimplePath;
            _pathActive = true;
            
           _lastDistance = 0;
           _currentPathPoint = 0;
           
           _animator.SetFloat("InputVertical", 1);
        }
#endif
        [ActionGroup("PathPauseUnpause")]
        [Action(English:"Pause movement along the path",Russian:"Приостановить движение по маршруту",Chinese:"暫停沿著路徑移動",Korean:"경로를 따라 이동 일시정지")]
        public void PausePath()
        {
            if (_currentMovementType == MovementType.SimplePath)
            {
                _pathActive = false;
            }
        }
        
        [ActionGroup("PathPauseUnpause")]
        [Action(English:"Continue movement along the path",Russian:"Продолжить движение по маршруту",Chinese:"繼續沿著路徑移動",Korean:"이동 경로를 따라 계속 진행")]
        public void ContinuePath()
        {
            if (_currentMovementType == MovementType.SimplePath)
            {
                _pathActive = true;
            }
        }
        
        [Action(English:"Stop motion",Russian:"Остановить движение",Chinese:"停止移動",Korean:"스톱 모션")]
        public void StopMovement()
        {
            _animator.SetFloat("InputHorizontal", 0);
            _animator.SetFloat("InputVertical", 0);
            _animator.SetFloat("InputMagnitude", 0);
            
            _distanceMoved = 0;
            _distanceRequired = 0;
            _lastDistance = 0;
            _currentPathPoint = 0;
            
            _targetObject = null;
            _pathPoints = null;
            _pathActive = false;
            
            _currentMovementType = MovementType.None;
        }
        
        [Obsolete]
        public void PlayCustomAnimationOnce([UseValueList("VarwinBotCustomAnimationClips")] int clipId)
        {
            _repeatAnimation = false;
            
            PlayClipWithId(clipId);
        } 
        
        [Obsolete]
        public void PlayCustomAnimationRepeatedly([UseValueList("VarwinBotCustomAnimationClips")] int clipId)
        {
            _repeatAnimation = true;
            
            PlayClipWithId(clipId);
        }
        
        [Obsolete]
        public void StopCustomAnimation()
        {
            _playableGraph.Stop();
        }
        
        [Action(English:"Set text bubble hide type",Russian:"Задать тип скрывания говоримого текста",Chinese:"設定文字泡泡隱藏類型",Korean:"말풍선 숨기기 유형 설정")]
        public void SetShowTextBubbleHideType(TextBubbleHideType hideType)
        {
            _textBubble.HideType = hideType;
        }

        [Action(English:"Set text bubble enabled",Russian:"Задать отображение говоримого текста",Chinese:"設定啟用文字泡泡",Korean:"말풍선 활성화 설정")]
        public void SetShowTextBubble(bool show)
        {
            _textBubble.ShowTextBubble = show;
        }

        [VarwinInspector(English:"Show text bubble",Russian:"Отображать говоримый текст",Chinese:"顯示文字泡泡",Korean:"말풍선 표시")]
        public bool ShowTextBubble
        {
            get =>  _textBubble ? _textBubble && _textBubble.ShowTextBubble : true;
            set
            {
                if (_textBubble)
                {
                    _textBubble.ShowTextBubble = value;
                }
            }
        }
        
        [VarwinInspector(English:"Hide text automatically",Russian:"Скрывать текст автоматически",Chinese:"自動隱藏文字",Korean:"텍스트 자동 숨기기")]
        public bool AutoHideTextBubble
        {
            get => _textBubble ? _textBubble.HideType == TextBubbleHideType.Automatic : true;
            set
            {
                if (_textBubble)
                {
                    _textBubble.HideType = value ? TextBubbleHideType.Automatic : TextBubbleHideType.Never;
                }
            }
        }
        
        [VarwinInspector(English:"The text bubble looks at the Player",Russian:"Отображаемый текст следит за игроком",Chinese:"文字氣泡看著玩家",Korean:"플레이어 방향을 바라보는 말풍선")]
        public bool TextBubbleLookAtCamera
        {
            get =>  _textBubble && (_textBubble && _textBubble.LookAtCamera);
            set
            {
                if (_textBubble)
                {
                    _textBubble.LookAtCamera = value;
                }
            }
        }
        
        [LogicEvent(English:"On speech completed",Russian:"Фраза произнесена",Chinese:"當演說結束",Korean:"대사가 끝났을 때")]
        [EventCustomSender(English:"Bot",Russian:"Бот",Chinese:"機器人",Korean:"봇")]
        public event Action SpeechCompleted;
        
        [Action(English:"Say",Russian:"Сказать",Chinese:"說",Korean:"말하기")]
        public void SayText(string text)
        {
            if (_textToSpeech)
            {
                _textToSpeech.SayText(text);
            }

            if (_textBubble)
            {
                _textBubble.ShowText(">", text);
            }
        }
        
        [Action(English:"Say",Russian:"Сказать",Chinese:"說",Korean:"말하기")]
        [ArgsFormat(English:"header: {%} text: {%}",Russian:"заголовок: {%} текст: {%}",Chinese:"標題: {%} 文字: {%}",Korean:"헤더: {%} 텍스트: {%}")]
        public void SayTextWith(string header, string text)
        {
            if (_textToSpeech)
            {
                _textToSpeech.SayText(text);
            }

            if (_textBubble)
            {
                _textBubble.ShowText(header, text);
            }
        }
        
        [Action(English:"Stop speaking",Russian:"Перестать говорить",Chinese:"停止說",Korean:"말하기 중지")]
        public void StopSpeaking()
        {
            if (_textToSpeech)
            {
                _textToSpeech.StopSpeaking();
            }

            if (_textBubble)
            {
                _textBubble.HideText();
            }
        }

        private void Awake()
        {
            _textToSpeech = GetComponent<VarwinBotTextToSpeech>();
            _textBubble = GetComponent<VarwinBotTextBubble>();
        }

        void Start()
        {
            _currentMovementType = MovementType.None;
            
            _animator = GetComponent<Animator>();
            _rigidbody = GetComponent<Rigidbody>();
            _characterController = GetComponent<CharacterController>();

            if (!_useCustomAnimatorController)
            {
                _animator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("AnimatorControllers/VarwinBotController");
            }
            
            _animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
            
            _rigidbody.useGravity = false;
            _rigidbody.isKinematic = true;
            
            _playableGraph = PlayableGraph.Create();
            _playableGraph.SetTimeUpdateMode(DirectorUpdateMode.GameTime);

            _animationPlayableOutput = AnimationPlayableOutput.Create(_playableGraph, "Animation", _animator);

            _clipPlayables = new List<AnimationClipPlayable>();

            foreach (VarwinBotCustomAnimation customAnimation in CustomAnimations)
            {
                var clipPlayable = AnimationClipPlayable.Create(_playableGraph, customAnimation.Clip);
                _clipPlayables.Add(clipPlayable);
            }
            
            _groundCheckMaxDistance = _characterController.height;

            if (!_textBubble)
            {
                GameObject bubbleObject = Instantiate(Resources.Load<GameObject>("TextBubble"));
                
                Transform head = _animator.GetBoneTransform(HumanBodyBones.Head);

                if (head)
                {
                    bubbleObject.transform.position = head.position + Vector3.up * 0.3f;
                }
                else
                {
                    bubbleObject.transform.position = transform.position + Vector3.up * 2.0f;
                }

                bubbleObject.transform.rotation = transform.rotation;
                
                bubbleObject.transform.SetParent(transform);

                _textBubble = gameObject.AddComponent<VarwinBotTextBubble>();
                _textBubble.Container = bubbleObject;
                _textBubble.HeaderText = bubbleObject.transform.Find("Canvas").Find("Header").GetComponent<TMP_Text>();
                _textBubble.MainText = bubbleObject.transform.Find("Canvas").Find("Text").GetComponent<TMP_Text>();
                _textBubble.ResetDefaultScale();
            }
            
            _textBubble.HideText();
            
            if (_textToSpeech)
            {
                _textBubble.BotHasTextToSpeech = true;

                _textToSpeech.SpeechCompleted += () =>
                {
                    _textBubble.BotTextToSpeechFinished = true;
                    SpeechCompleted?.Invoke();
                };
            }
            else
            {
                _textBubble.SpeechCompleted += () => SpeechCompleted?.Invoke();
            }
        }

        private void Update()
        {
            Ray ray = new Ray(transform.position + 0.2f * _characterController.height * transform.localScale.y * Vector3.up, Vector3.down);
            RaycastHit groundHit;
            
            bool isGrounded;

            if (Physics.Raycast(ray, out groundHit, _groundCheckMaxDistance))
            {
                float groundDistance = Vector3.Distance(transform.position, groundHit.point);

                isGrounded = (groundDistance <= 0.5f * _characterController.height * transform.localScale.y);
            }
            else
            {
                isGrounded = false;
            }
            
            _animator.SetBool("IsGrounded", isGrounded);
            
            if (Mathf.Abs(_angularVelocity) > 0)
            {
                transform.Rotate(transform.up, Time.deltaTime * _angularVelocity);
                _currentRotationTime += Time.deltaTime;

                if (_rotationTime - _currentRotationTime < Time.deltaTime)
                {
                    transform.Rotate(transform.up, (_rotationTime - _currentRotationTime) * _angularVelocity);

                    _angularVelocity = 0;
                    _rotationTime = 0;
                    _currentRotationTime = 0;
                }
            }

            if (_playableGraph.IsPlaying())
            {
                float timeDiff = Mathf.Abs((float) _clipPlayables[_animationClipId].GetTime() - CustomAnimations[_animationClipId].Clip.length);

                if (timeDiff < Time.deltaTime)
                {
                    if (_repeatAnimation)
                    {
                        _clipPlayables[_animationClipId].SetTime(0);
                    }
                    else
                    {
                        _playableGraph.Stop();
                    }
                }
            }
            
            if (_currentMovementType == MovementType.ForMeters)
            {
                _distanceMoved += Vector3.Distance(_lastPosition, transform.position);

                if (_distanceMoved >= _distanceRequired)
                {
                    StopMovement();
                }

                _lastPosition = transform.position;
            }
            else if (_currentMovementType == MovementType.TowardsObject && _targetObject)
            {
                Vector3 targetObjectPosition = _targetObject.transform.position;

                float distance = Vector3.Distance(targetObjectPosition - Vector3.up * targetObjectPosition.y, transform.position - Vector3.up * transform.position.y);

                Vector3 targetPosition = new Vector3(targetObjectPosition.x, transform.position.y, targetObjectPosition.z);

                transform.LookAt(targetPosition);
                
                if (distance <= _targetObjectMinDistance)
                {
                    if (_lastDistance > _targetObjectMinDistance)
                    {
                        Wrapper eventObjectWrapper = _targetObject.GetWrapper();
                        
                        StopMovement();
                        
                        BotTargetReached?.Invoke(eventObjectWrapper);
                    }
                }
                else
                {
                    float inputMagnitude = 0;

                    switch (_currentMovementPace)
                    {
                        case MovementPace.Walk:
                            inputMagnitude = 0.5f;

                            break;
                        case MovementPace.Run:
                            inputMagnitude = 1f;

                            break;
                    }
                    
                    _animator.SetFloat("InputMagnitude", inputMagnitude);
                }

                _lastDistance = distance;
            }
            else if (_currentMovementType == MovementType.SimplePath)
            {
                if (_pathPoints == null || _pathPoints.Count == 0)
                {
                    StopMovement();
                }
                
                Vector3 targetObjectPosition = _pathPoints[_currentPathPoint].position;

                float distance = Vector3.Distance(targetObjectPosition - Vector3.up * targetObjectPosition.y, transform.position - Vector3.up * transform.position.y);

                Vector3 targetPosition = new Vector3(targetObjectPosition.x, transform.position.y, targetObjectPosition.z);

                transform.LookAt(targetPosition);
                
                if (distance <= _targetObjectMinDistance)
                {
                    if (_lastDistance > _targetObjectMinDistance)
                    {
                        int eventPathPoint = _currentPathPoint + 1;
                        Wrapper eventObjectWrapper = _pathPoints[_currentPathPoint].gameObject.GetWrapper();
                        
                        _currentPathPoint++;
                        
                        if (_currentPathPoint >= _pathPoints.Count)
                        {
                            StopMovement();
                        }
                        
                        BotPathPointReached?.Invoke(eventPathPoint, eventObjectWrapper);
                    }
                }
                else
                {
                    float inputMagnitude = 0;

                    if (_pathActive)
                    {
                        switch (_currentMovementPace)
                        {
                            case MovementPace.Walk:
                                inputMagnitude = 0.5f;

                                break;
                            case MovementPace.Run:
                                inputMagnitude = 1f;

                                break;
                        }
                    }
                    else
                    {
                        inputMagnitude = 0;
                    }
                    
                    _animator.SetFloat("InputMagnitude", inputMagnitude);
                }

                _lastDistance = distance;
            }

        }

        public void OnSwitchMode(GameMode newMode, GameMode oldMode)
        {
            if (newMode == GameMode.Edit)
            {
                if (_textToSpeech)
                {
                    _textToSpeech.SetSpeechState(false);
                }

                if (_textBubble)
                {
                    _textBubble.HideText();
                }

                StopMovement();
                
                _animator.applyRootMotion = false;
            }
            else
            {
                if (_textToSpeech)
                {
                    _textToSpeech.SetSpeechState(true);
                }
                
                if (_textBubble)
                {
                    _textBubble.ResetDefaultScale();
                }
                
                _animator.applyRootMotion = true;
            }
        }
        
        public void MoveBot(MovementDirection movementDirection, MovementPace movementPace)
        {
            _currentMovementDirection = movementDirection;
            _currentMovementPace = movementPace;
            float inputMagnitude = 0;
            float inputHorizontal = 0;
            float inputVertical = 0;

            switch (movementPace)
            {
                case MovementPace.Walk:
                    inputMagnitude = 0.5f;
                    break;
                case MovementPace.Run:
                    inputMagnitude = 1f;
                    break;
            }

            switch (movementDirection)
            {
                case MovementDirection.Forward:
                    inputVertical = 1f;
                    break;
                case MovementDirection.Backward:
                    inputVertical = -1f;
                    break;
                case MovementDirection.Left:
                    inputHorizontal = -1f;
                    break;
                case MovementDirection.Right:
                    inputHorizontal = 1f;
                    break;
            }
            
            _animator.SetFloat("InputHorizontal", inputHorizontal);
            _animator.SetFloat("InputVertical", inputVertical);
            _animator.SetFloat("InputMagnitude", inputMagnitude);
        }

        [Obsolete]
        private void PlayClipWithId(int clipId)
        {
            if (clipId == -1)
            {
                return;
            }
            
            _animationClipId = clipId;
            
            _animationPlayableOutput.SetSourcePlayable(_clipPlayables[_animationClipId]);
            _clipPlayables[_animationClipId].SetTime(0);
            _playableGraph.Play();
        }

        #region BACKWARD COMPATIBILITY CODE
        [Obsolete]
        public List<VarwinCustomAnimation> GetCustomAnimations()
        {
            List <VarwinCustomAnimation> customAnimations = new List<VarwinCustomAnimation>();
            foreach (var customAnimation in  CustomAnimations)
            {
                customAnimations.Add(customAnimation);
            }
            return customAnimations;
        }
        [Obsolete]
        public string GetCustomAnimationsValueListName()
        {
            return "VarwinBotCustomAnimationClips";
        }
        #endregion


    }
}