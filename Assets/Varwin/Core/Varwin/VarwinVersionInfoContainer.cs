public static class VarwinVersionInfoContainer
    {
    	public static string VersionNumber => "17.7.77";
    	public static string VersionString => "Version 17.7.77";
    }
