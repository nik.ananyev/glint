﻿using System.Collections.Generic;
using UnityEngine;

namespace Varwin
{
    /// <summary>
    /// Класс-хэлпер для добавления и удаления коллайдеров, коллизия с которыми будет игнорироваться при затемнении головы.
    /// </summary>
    public static class HeadCollisionFadeHelper
    {
        /// <summary>
        /// Список игнорируемых коллайдеров.
        /// </summary>
        private static readonly HashSet<Collider> HeadCollisionFadeIgnoredColliders = new();

        /// <summary>
        /// Подписка на загрузку сцены.
        /// </summary>
        static HeadCollisionFadeHelper()
        {
            ProjectData.SceneCleared += Clear;
        }

        /// <summary>
        /// Очистка всех игнорируемых коллайдеров.
        /// </summary>
        private static void Clear()
        {
            HeadCollisionFadeIgnoredColliders.Clear();
        }

        /// <summary>
        /// Проверка, является ли коллайдер игнорируемым.
        /// </summary>
        /// <param name="collider">Коллайдер.</param>
        /// <returns>true - коллайдер нужно игнорировать. false - коллайдер не нужно игнорировать.</returns>
        public static bool IsIgnoredCollider(Collider collider)
        {
            return HeadCollisionFadeIgnoredColliders.Contains(collider);
        }

        /// <summary>
        /// Добавить коллайдер к списку игнорируемых коллайдеров.
        /// </summary>
        /// <param name="collider">Коллайдер.</param>
        public static void AddColliderToIgnore(Collider collider)
        {
            HeadCollisionFadeIgnoredColliders.Add(collider);
        }

        /// <summary>
        /// Удалить коллайдер из списка игнорируемых коллайдеров.
        /// </summary>
        /// <param name="collider">Коллайдер.</param>
        public static void RemoveIgnoredCollider(Collider collider)
        {
            if (HeadCollisionFadeIgnoredColliders.Contains(collider))
            {
                HeadCollisionFadeIgnoredColliders.Remove(collider);
            }
        }
    }
}