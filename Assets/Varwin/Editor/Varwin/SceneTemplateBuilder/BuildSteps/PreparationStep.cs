using System.IO;
using UnityEditor;
using UnityEngine;
using Varwin.Editor;

namespace Varwin.SceneTemplateBuilding
{
    public class PreparationStep : BaseSceneTemplateBuildStep
    {
        public PreparationStep(SceneTemplateBuilder builder) : base(builder)
        {
        }
        
        public override void Update()
        {
            base.Update();
            
            if (!Directory.Exists(Builder.DestinationFolder))
            {
                try
                {
                    Directory.CreateDirectory(Builder.DestinationFolder);
                }
                catch
                {
                    var message = string.Format(SdkTexts.CannotCreateDirectoryFormat, Builder.DestinationFolder);
                    Debug.LogError(message);
                    
                    EditorUtility.DisplayDialog(SdkTexts.CannotCreateDirectoryTitle, message, "OK");
                    throw;
                }
            }
        }
    }
}