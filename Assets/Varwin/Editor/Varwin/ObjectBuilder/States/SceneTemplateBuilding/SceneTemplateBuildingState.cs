using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using Varwin.SceneTemplateBuilding;
using Object = UnityEngine.Object;

namespace Varwin.Editor
{
    public class SceneTemplateBuildingState : BaseSceneTemplateBuildingState
    {
        private SceneTemplateBuilder _sceneTemplateBuilder;
        
        public SceneTemplateBuildingState(VarwinBuilder builder) : base(builder)
        {
            Label = "Scene template building";
        }

        protected override void OnEnter()
        {
            Builder.Serialize();
        }

        protected override void Update(SceneTemplateBuildDescription sceneTemplateBuildDescription)
        {
            try
            {
                if (CurrentIndex < Builder.CurrentBuildingSceneIndex)
                {
                    return;
                }
                
                EditorSceneManager.OpenScene(sceneTemplateBuildDescription.Path);
                sceneTemplateBuildDescription.Name = Path.GetFileNameWithoutExtension(sceneTemplateBuildDescription.Path);
                sceneTemplateBuildDescription.RootGuid = Object.FindObjectOfType<WorldDescriptor>().RootGuid;
                CreateSceneTemplateWindow.PrepareSceneForBuild();
                sceneTemplateBuildDescription.NewGuid = Object.FindObjectOfType<WorldDescriptor>().Guid;
                _sceneTemplateBuilder = CreateSceneTemplateWindow.GetSceneTemplateBuilder();
                _sceneTemplateBuilder.PrepareBuildSteps();
                Builder.CurrentBuildingSceneIndex++;
                Builder.Serialize();
                _sceneTemplateBuilder.BuildImmediate();
            }
            catch (Exception e)
            {
                sceneTemplateBuildDescription.HasError = true;
                Debug.LogError($"Scene Template Building Error:\n{e}");
            }
        }
    }
}