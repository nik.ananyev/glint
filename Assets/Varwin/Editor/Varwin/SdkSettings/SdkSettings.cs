using UnityEditor;

namespace Varwin.Editor
{
    /// <summary>
    ///     SDK global settings class
    /// </summary>
    public static class SdkSettings
    {
        public const string ObjectCreationFolderPathPrefKey = @"SDKSettings.ObjectCreationFolderPath";
        public const string ObjectBuildingFolderPathPrefKey = @"SDKSettings.ObjectBuildingFolderPath";
        public const string SceneTemplateBuildingFolderPathPrefKey = @"SDKSettings.SceneTemplateBuildingFolderPath";

        public static string ObjectCreationFolderPath = EditorPrefs.GetString(ObjectCreationFolderPathPrefKey, "Assets/Objects");
        public static string ObjectBuildingFolderPath = EditorPrefs.GetString(ObjectBuildingFolderPathPrefKey, "BakedObjects");
        public static string SceneTemplateBuildingFolderPath = EditorPrefs.GetString(SceneTemplateBuildingFolderPathPrefKey, "BakedSceneTemplates");

        public static class Features
        {
            public static readonly SdkFeature DeveloperMode = new("Developer mode");
            public static readonly SdkFeature Mobile = new("Mobile", true);
            public static readonly SdkFeature Linux = new("Linux", true);
            public static readonly SdkFeature WebGL = new("WebGL");
            public static readonly SdkFeature DynamicVersioning = new("Dynamic versioning", true);
            public static readonly SdkFeature Changelog = new("Changelog", true);
            
            public static readonly SdkFeature OverrideDefaultObjectSettings = new("Override default VarwinObjectDescriptor settings", false);
            public static readonly SdkFeature AddBehavioursAtRuntime = new("Add behaviours at runtime", false);
            public static readonly SdkFeature MobileReady = new("Mobile ready", false);
            public static readonly SdkFeature SourcesIncluded = new("Sources included", false);
            public static readonly SdkFeature DisableSceneLogic = new("Disable scene logic", false);
        }
    }
}