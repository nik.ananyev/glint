using System;
using UnityEditor;
using UnityEngine;

namespace Varwin.Editor
{
    [Serializable]
    public class SdkFeature
    {
        [SerializeField]
        public string Name;

        [SerializeField]
        public bool Enabled;
        
        public string Key => $"SDKFeature.{Name}";

        public SdkFeature(string name)
        {
            Name = name;
            Enabled = Load();
        }

        public SdkFeature(string name, bool defaultValue)
        {
            Name = name;
            if (EditorPrefs.HasKey(Key))
            {
                Enabled = Load();
            }
            else
            {
                Enabled = defaultValue;
                Save();
            }
        }
        
        public bool Load()
        {
            if (!EditorPrefs.HasKey(Key))
            {
                EditorPrefs.SetBool(Key, false);
            }
            return EditorPrefs.GetBool(Key, false);
        }

        public event Action<bool> OnSave;
        
        public virtual void Save()
        {
            OnSave?.Invoke(Enabled);   
            EditorPrefs.SetBool(Key, Enabled);
        }
        
        /// <summary>
        /// SDK Feature exists and enabled
        /// </summary>
        /// <param name="feature">SDK Feature</param>
        /// <returns></returns>
        public static implicit operator bool(SdkFeature feature) => feature is { Enabled: true };
    }
}
