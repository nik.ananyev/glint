#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Varwin.Public;
using static UnityEditor.EditorGUILayout;

namespace Varwin.Tools
{
    public class VarwinMethodInvoker : EditorWindow
    {
        private const int SplitLineLength = 250;

        private static string _splitLineString = new String('_', SplitLineLength);
        
        private GameObject _previousObject;
        private GameObject _currentObject;

        private Vector2 _scrollPosition;
        private bool _onlyVarwinMethods;
        private int _excludeMaskInt;
        private int _excludeMask;
        private int _previousExcludeMask;

        private Dictionary<ParameterInfo, dynamic> _parameters;
        private Dictionary<MethodData, Vector2> _parametersScrolls;
        private Color _headerColor = Color.Lerp(Color.gray, Color.white, 0.1f);

        private MethodInfo[] _monoBehavioursMethods;
        private MethodInfo[] _varwinObjectsMethods;
        private List<MethodInfo[]> _methodByExcludeTypes;

        private HashSet<MonoBehaviour> _monoBehavioursHash;
        private HashSet<MethodInfo> _filteredMonoBehaviours;
        private string _searchString;
        private bool _changed;

        private HashSet<Type> _excludeTypes = new()
        {
            typeof(MeshFilter),
            typeof(MeshRenderer),
            typeof(Collider),
            typeof(Transform)
        };

        [MenuItem("Varwin/Tools/Method Invoker")]
        private static void ShowWindow()
        {
            var window = (VarwinMethodInvoker) GetWindow(typeof(VarwinMethodInvoker));
            window.name = "Varwin Method Invoker";
            window.Show();
        }

        private void OnGUI()
        {
            if (_methodByExcludeTypes == null)
            {
                _monoBehavioursMethods = typeof(MonoBehaviour).GetMethods();
                _varwinObjectsMethods = typeof(VarwinObject).GetMethods();
                _methodByExcludeTypes = new()
                {
                    _monoBehavioursMethods,
                    _varwinObjectsMethods
                };
            }

            Redraw();
        }

        private void Redraw()
        {
            DrawObjectField();
            _excludeMask = LayerMaskField("Exclude Methods", _excludeMaskInt);
            _searchString = DrawSearchField();

            if (!_currentObject)
            {
                return;
            }

            var normalColor = GUI.color;
            var normalBackColor = GUI.backgroundColor;
            GUI.color = _headerColor;

            BeginHorizontal();
            DrawLabel("Return Type", GUILayout.Width(150));
            DrawLabel("Invoke Method", GUILayout.Width(200));
            DrawLabel("Parameters", GUILayout.Width(200));
            EndHorizontal();

            GUI.color = normalColor;

            if (_monoBehavioursHash == null || _currentObject != _previousObject)
            {
                _monoBehavioursHash = _currentObject.GetComponentsInChildren<MonoBehaviour>().ToHashSet();
            }

            _scrollPosition = BeginScrollView(_scrollPosition);

            BeginVertical();
            foreach (var monoBehaviour in _monoBehavioursHash)
            {
                if (!monoBehaviour)
                {
                    continue;
                }

                var type = monoBehaviour.GetType();
                if (_excludeTypes.Contains(type))
                {
                    continue;
                }

                _filteredMonoBehaviours = type.GetMethods(BindingFlags.Instance | BindingFlags.Public).ToHashSet();
                for (int filterIndex = 0; filterIndex < _methodByExcludeTypes.Count; filterIndex++)
                {
                    if ((_excludeMask & (1 << filterIndex)) > 0)
                    {
                        var excludeTypes = _methodByExcludeTypes[filterIndex].Select(x => x.Name);
                        _filteredMonoBehaviours = _filteredMonoBehaviours.Where(x => !excludeTypes.Contains(x.Name)).ToHashSet();
                    }
                }

                if (_filteredMonoBehaviours.Count == 0)
                {
                    continue;
                }

                var componentName = monoBehaviour.GetType().Name;

                bool SearchFilter(string methodName) => !string.IsNullOrEmpty(methodName) && methodName.Contains(_searchString);

                var methodsData = string.IsNullOrEmpty(_searchString)
                    ? _filteredMonoBehaviours.Select(GetMethodData).ToList()
                    : _filteredMonoBehaviours.Where(method => SearchFilter(method.Name)).Select(GetMethodData).ToList();

                if (methodsData.Any())
                {
                    DrawComponentHeader(componentName);

                    foreach (var methodData in methodsData)
                    {
                        DrawMethodInfo(methodData, monoBehaviour);
                    }
                }
            }

            EndVertical();
            EndScrollView();

            GUI.color = normalColor;
            GUI.backgroundColor = normalBackColor;
            _previousObject = _currentObject;
        }

        private void DrawComponentHeader(string componentName)
        {
            SplitLine();
            GUILayout.Label(componentName);
        }

        private void SplitLine() => DrawLabel(_splitLineString);

        private string DrawSearchField() => TextField("Search:", _searchString);

        private void DrawObjectField() => _currentObject = (GameObject) ObjectField("Object", _currentObject, typeof(GameObject), true);

        private void DrawLabel(string monoBehaviourName, params GUILayoutOption[] guiStyle) => GUILayout.Label(monoBehaviourName, guiStyle);

        private void DrawMethodInfo(MethodData methodData, MonoBehaviour monoBehaviour)
        {
            BeginHorizontal();
            DrawLabel(methodData.ReturnType.Name, GUILayout.Width(150));

            if (GUILayout.Button(methodData.Name, GUILayout.Width(200)))
            {
                object[] parameters = new object[methodData.MethodParameters.Length];

                for (var i = 0; i < methodData.MethodParameters.Length; i++)
                {
                    parameters[i] = _parameters[methodData.MethodParameters[i]];
                }

                if (methodData.ReturnType != typeof(void))
                {
                    if(methodData.ReturnType == typeof(IEnumerator))
                    {
                        monoBehaviour.StartCoroutine((IEnumerator) methodData.MethodInfo.Invoke(monoBehaviour, parameters));
                    }
                    else
                    {
                        object result = methodData.MethodInfo.Invoke(monoBehaviour, parameters);
                        Debug.Log($"<color=#00FF00>Invoke {methodData.Name}: {result}</color>");
                    }
                }
                else
                {
                    methodData.MethodInfo.Invoke(monoBehaviour, parameters);
                }
            }

            DrawParameters(methodData);
            EndHorizontal();
        }

        private void DrawParameters(MethodData methodData)
        {
            var parameters = methodData.MethodParameters;

            //BeginHorizontal(GUILayout.MaxHeight(20));

            if (parameters == null || parameters.Length == 0)
            {
                //EndHorizontal();
                return;
            }

            if (_parameters == null)
            {
                _parameters = new();
            }

            float originalValue = EditorGUIUtility.labelWidth;
            bool originalWideMode = EditorGUIUtility.wideMode;
            EditorGUIUtility.wideMode = true;

            foreach (var parameterInfo in parameters)
            {
                var type = parameterInfo.ParameterType;
                dynamic parameterValue = null;
                float width = EditorStyles.label.CalcSize(new GUIContent(parameterInfo.Name)).x;
                var paramWidth = 250 - width;
                EditorGUIUtility.labelWidth = width;

                if (type == typeof(int) || type == typeof(Int32))
                {
                    parameterValue = _parameters.ContainsKey(parameterInfo) ? _parameters[parameterInfo] : 0;
                    parameterValue = IntField(parameterInfo.Name, parameterValue, GUILayout.Width(paramWidth), GUILayout.MaxHeight(20));
                    _parameters[parameterInfo] = parameterValue;
                }
                else if (type == typeof(float))
                {
                    parameterValue = _parameters.ContainsKey(parameterInfo) ? _parameters[parameterInfo] : 0.0f;
                    parameterValue = FloatField(parameterInfo.Name, parameterValue, GUILayout.Width(paramWidth), GUILayout.MaxHeight(20));
                    _parameters[parameterInfo] = parameterValue;
                }
                else if (type == typeof(string))
                {
                    parameterValue = _parameters.ContainsKey(parameterInfo) ? _parameters[parameterInfo] : "";
                    parameterValue = TextField(parameterInfo.Name, parameterValue, GUILayout.Width(paramWidth), GUILayout.MaxHeight(20));
                    _parameters[parameterInfo] = parameterValue;
                }
                else if (type == typeof(GameObject))
                {
                    parameterValue = _parameters.ContainsKey(parameterInfo) ? _parameters[parameterInfo] : null;
                    parameterValue = ObjectField(parameterInfo.Name, parameterValue, typeof(GameObject), GUILayout.Width(paramWidth), GUILayout.MaxHeight(20));
                    _parameters[parameterInfo] = parameterValue;
                }
                else if (type == typeof(Vector2))
                {
                    parameterValue = _parameters.ContainsKey(parameterInfo) ? _parameters[parameterInfo] : Vector2.zero;
                    parameterValue = Vector2Field(parameterInfo.Name, parameterValue, GUILayout.Width(paramWidth), GUILayout.MaxHeight(20));
                    _parameters[parameterInfo] = parameterValue;
                }
                else if (type == typeof(Vector3))
                {
                    parameterValue = _parameters.ContainsKey(parameterInfo) ? _parameters[parameterInfo] : Vector3.zero;
                    parameterValue = Vector3Field(parameterInfo.Name, parameterValue, GUILayout.Width(paramWidth), GUILayout.MaxHeight(20));
                    _parameters[parameterInfo] = parameterValue;
                }
                else if (type == typeof(Vector4))
                {
                    parameterValue = _parameters.ContainsKey(parameterInfo) ? _parameters[parameterInfo] : Vector4.zero;
                    parameterValue = Vector4Field(parameterInfo.Name, parameterValue, GUILayout.Width(paramWidth), GUILayout.MaxHeight(20));
                    _parameters[parameterInfo] = parameterValue;
                }
                else if (type == typeof(Color))
                {
                    parameterValue = _parameters.ContainsKey(parameterInfo) ? _parameters[parameterInfo] : Color.white;
                    parameterValue = ColorField(parameterInfo.Name, parameterValue, GUILayout.Width(paramWidth), GUILayout.MaxHeight(20));
                    _parameters[parameterInfo] = parameterValue;
                }
                else if (type.IsEnum)
                {
                    parameterValue = _parameters.ContainsKey(parameterInfo) ? _parameters[parameterInfo] : 0;

                    parameterValue = EnumPopup(parameterInfo.Name, Enum.ToObject(type, parameterValue), GUILayout.Width(paramWidth), GUILayout.MaxHeight(20));
                    _parameters[parameterInfo] = parameterValue;
                }
                else if (type == typeof(bool))
                {
                    parameterValue = _parameters.ContainsKey(parameterInfo) ? _parameters[parameterInfo] : false;
                    parameterValue = Toggle(parameterInfo.Name, parameterValue, GUILayout.Width(paramWidth), GUILayout.MaxHeight(20));
                    _parameters[parameterInfo] = parameterValue;
                }
            }

            EditorGUIUtility.labelWidth = originalValue;
            EditorGUIUtility.wideMode = originalWideMode;

            //EndHorizontal();
        }

        private MethodData GetMethodData(MethodInfo methodInfo) => new()
        {
            Name = methodInfo.Name,
            ReturnType = methodInfo.ReturnType,
            MethodParameters = methodInfo.GetParameters(),
            MethodInfo = methodInfo
        };

        private LayerMask LayerMaskField(string label, LayerMask layerMask)
        {
            string[] layers =
            {
                "MonoBehaviour",
                "VarwinObject"
            };

            List<int> layerNumbers = new List<int>
            {
                0, // MonoBehaviour
                1 // VarwinObject
            };

            for (int i = 0; i < layerNumbers.Count; i++)
            {
                if (((1 << layerNumbers[i]) & layerMask.value) > 0)
                {
                    _excludeMaskInt |= (1 << i);
                }
            }

            _excludeMaskInt = MaskField(label, _excludeMaskInt, layers);
            int mask = 0;
            for (int i = 0; i < layerNumbers.Count; i++)
            {
                if ((_excludeMaskInt & (1 << i)) > 0)
                {
                    mask |= (1 << layerNumbers[i]);
                }
            }

            layerMask.value = mask;
            return layerMask;
        }
    }

    public struct MethodData
    {
        public Type ReturnType;
        public ParameterInfo[] MethodParameters;
        public string Name;
        public MethodInfo MethodInfo;
    }
}
#endif